﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Mo2._1.Controllers
{
    public class ar : Controller
    {
        // GET: ar
        public ActionResult entp()
        {
            return View();
        }

        // GET: ar/Details/5
        public ActionResult entj()
        {
            return View();
        }

        public ActionResult intp()
        {
            return View();
        }

        public ActionResult intj()
        {
            return View();
        }

        public ActionResult enfp()
        {
            return View();
        }

        public ActionResult enfj()
        {
            return View();
        }


        // GET: ar/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ar/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ar/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ar/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ar/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ar/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
